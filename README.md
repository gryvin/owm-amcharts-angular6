SPA для запроса погоды с OpenWeatherMap и визуализации на карте\
Сделано на Angular6 и библиотеке amMaps (amcharts.com)\
Работает с тачскрином (плохо, но работает)\
# Функционал:\
	1. ввод названия города + Enter = список погоды в одноименных городах по всему миру\
	2. выбор страны на карте + название города + Enter = погода в одноименных городах выбранной страны
	3. найденные города отмечаются на карте (с глюками)\
	4. tooltip города на карте показывает погоду детально\

OpenWeatherMap реализует "интеллектуальный" поиск городов, поэтому результаты м.б.
неочевидны (например, если набрать "Москва" по-русски - найдёт столицу нашей родины,
если "Moscow" - найдёт "Маскву" в Казахстане и несколько по всему миру)


# OWMAmchartsAngular6

   This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.2.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

