import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as Rx from 'rxjs';

@Injectable()
export class OwmService {
  
  private _wi = new Rx.BehaviorSubject<WeatherInfo[]>([]);
  public wi = this._wi.asObservable()

  constructor(private http: HttpClient) { }
  currCountry: string
  getCityToday(cityName){   
    let  todayWeather  = new Array<WeatherInfo>()
    this.http.get(
      "http://openweathermap.org/data/2.5/find?type=like&sort=population&units=metric&cnt=30&appid=b6907d289e10d714a6e88b30761fae22&_=1537819882056&q="
      +cityName
      +((this.currCountry && this.currCountry.length ==2 )?(","+this.currCountry):"")
    ).subscribe((data: WeatherToday) => {
      if(data.cod == "200"){                
        data.list.forEach(item => {
          let wi = new WeatherInfo
          wi.cityId = item.id
          wi.cityName = item.name
          wi.countryCode = item.sys.country
          wi.humidity = item.main.humidity
          wi.pressure = item.main.pressure
          wi.temp = Math.round((item.main.temp  - 273.15) * 10) / 10
          wi.icon = item.weather[0].icon
          wi.latitude = item.coord.lat
          wi.longitude = item.coord.lon
          todayWeather.push(wi)          
        })
      }      
      console.log("service", this.currCountry, todayWeather)
      this._wi.next(todayWeather);
     });
  }
  
}

export interface WeatherToday{
  cod: string
  count: number
  list: [
    {
      coord: {lat: number, lon: number}
      id: number
      main: {
        humidity: number
        pressure: number
        temp: number
      }
      name: string
      sys: {country: string}
      weather: [{
        description: string
        icon: string
        id: number
      }]
    }
  ]

}
export class WeatherInfo{
  countryCode: string
  cityName: string
  cityId: number
  icon: string
  humidity: number
  pressure: number
  temp: number
  latitude: number
  longitude: number
}
