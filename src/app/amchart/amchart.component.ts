import { Component, NgZone } from "@angular/core";
import * as am4core from "@amcharts/amcharts4/core";
import { OwmService } from '../owm.service';
import * as am4maps from "@amcharts/amcharts4/maps";
import am4geodata_worldLow from "@amcharts/amcharts4-geodata/worldLow";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import { extend } from "webdriver-js-extender";

am4core.useTheme(am4themes_animated);

@Component({
  selector: 'app-amchart',
  templateUrl: './amchart.component.html',
  styleUrls: ['./amchart.component.css']
})
export class AmchartComponent {
  chart
	citiesWeather 

  constructor(private zone: NgZone, private owm: OwmService) {} 

  ngAfterViewInit() {
    this.zone.runOutsideAngular(() => {

	let chart = am4core.create("map", am4maps.MapChart);

	try {
		chart.geodata = am4geodata_worldLow;
	}
	catch (e) {
		chart.raiseCriticalError({
    "name": "criticalError",
		"message": "Map geodata could not be loaded. Please download the latest <a href=\"https://www.amcharts.com/download/download-v4/\">amcharts geodata</a> and extract its contents into the same directory as your amCharts files."
		});
	}

	chart.projection = new am4maps.projections.Miller();

	let polygonSeries = chart.series.push(new am4maps.MapPolygonSeries());
	polygonSeries.useGeodata = true;
	polygonSeries.exclude = ["AQ"]; // Exclude Antractica
	polygonSeries.tooltip.fill = am4core.color("#000000");
	
	let colorSet = new am4core.ColorSet();

// Configure polygons
	let polygonTemplate = polygonSeries.mapPolygons.template;
	polygonTemplate.tooltipText = "{name}";
	polygonTemplate.togglable = true;

// Set events to apply "active" state to clicked polygons
	let currentActive
	let currentCode = "";
	polygonTemplate.events.on("hit", (event) => {
		let newCode = (event.target.dataItem as any)._dataContext.id;
    if(newCode == currentCode) {
			currentActive.setState("default");	
			currentCode = ""
		}else{
			if(currentActive) currentActive.setState("default");	
			chart.zoomToMapObject(event.target);
			currentActive = event.target;
			currentCode = newCode
		}
	  this.owm.currCountry = currentCode
	})

// Configure states
	let hoverState = polygonTemplate.states.create("hover");
	hoverState.properties.fill = colorSet.getIndex(0);

// Configure "active" state
	let activeState = polygonTemplate.states.create("active");
	activeState.properties.fill = colorSet.getIndex(4);

	var imageSeries = chart.series.push(new am4maps.MapImageSeries());
	var imageSeriesTemplate = imageSeries.mapImages.template;
	var circle = imageSeriesTemplate.createChild(am4core.Circle);
	circle.radius = 8;
	circle.fill = am4core.color("#B27799");
	circle.stroke = am4core.color("#FFFFFF");
	circle.strokeWidth = 1;
	circle.nonScaling = true;
	circle.tooltipHTML = `<img src='http://openweathermap.org/img/w/{icon}.png' style='position:absolute; top:-20px; right:-20px; width:80px; height: 80px;' />		
	<strong>{cityName} {countryCode}</strong>	</center>
	<hr /><table><tr>
  <th align="left">Температура</th>
  <td>{temp}</td>
	</tr><tr>
  <th align="left">Влажность</th>
  <td>{humidity}</td>
	</tr><tr>
  <th align="left">Давление</th>
  <td>{pressure}</td>
	</tr></table>	`;
	
// attempt to customize icon	
	// var targetSVG = "M9,0C4.029,0,0,4.029,0,9s4.029,9,9,9s9-4.029,9-9S13.971,0,9,0z M9,15.93 c-3.83,0-6.93-3.1-6.93-6.93S5.17,2.07,9,2.07s6.93,3.1,6.93,6.93S12.83,15.93,9,15.93 M12.5,9c0,1.933-1.567,3.5-3.5,3.5S5.5,10.933,5.5,9S7.067,5.5,9,5.5 S12.5,7.067,12.5,9z";
  // var clear ="M52 36 L58 30 L60 20 L56 10 L44 8 L34 12 L26 12 L18 10 L8 14 L2 24 L6 34 L14 38 L22 40 L32 38 L32 46 L36 52 L42 50 L42 44 L36 38 Z"
	// var img =  imageSeriesTemplate.createChild(am4core.PointedShape)
	// img.path = clear;


// Set property fields
	imageSeriesTemplate.propertyFields.latitude = "latitude";
	imageSeriesTemplate.propertyFields.longitude = "longitude";

	this.owm.wi.subscribe(			
		(res) => {
			console.log("amchart", res) 
			this.citiesWeather = res.map(q => q);
			console.log("amchart", this.citiesWeather) 
			imageSeries.data = this.citiesWeather;
		}
	)

      this.chart = chart;
    });
  }

  ngOnDestroy() {
    this.zone.runOutsideAngular(() => {
      if (this.chart) {
        this.chart.dispose();
      }
    });
  }
}
