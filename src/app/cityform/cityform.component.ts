import { Component, OnInit } from '@angular/core';
import { OwmService, WeatherInfo } from '../owm.service';
import * as Rx from 'rxjs';  

@Component({
  selector: 'app-cityform',
  templateUrl: './cityform.component.html',
  styleUrls: ['./cityform.component.css']
})
export class CityformComponent implements OnInit {
  
  todayWeather : WeatherInfo[];

  constructor(private owm: OwmService) { }

  getWeatherCities(cityName: string) {
    if(cityName.length > 0){
      this.owm.getCityToday(cityName)       
    } 
  }
  
  ngOnInit() {
    this.owm.wi.subscribe(
      (res) => {
        this.todayWeather = res.map(q => q);
        console.log("citycom", this.todayWeather) 
      }
    )
         
  }

}
