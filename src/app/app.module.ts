import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AmchartComponent } from './amchart/amchart.component';
import { CityformComponent } from './cityform/cityform.component';
import { HttpClientModule } from '@angular/common/http';
import { OwmService } from './owm.service';

@NgModule({
  declarations: [
    AppComponent,
    AmchartComponent,
    CityformComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [OwmService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
